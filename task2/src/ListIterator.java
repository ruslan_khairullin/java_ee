public interface ListIterator<L>{
    boolean hasNext();
    L next();
}
