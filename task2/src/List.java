public interface List<T> {
    T get(int index);
    void add(T element);
    ListIterator<T> iterator();

}
