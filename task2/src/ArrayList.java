public class ArrayList<E> implements List<E> {
    private final static int DEFAULT_SIZE = 10;
    private E elements[];
    int count;

    public ArrayList() {
        this.elements = (E[]) new Object[DEFAULT_SIZE];
        this.count = 0;
    }
    @Override
    public E get(int index) {
        if(index>=0 && index <=count) {
            return elements[index];
        }
        else {
            return null;
        }
    }
    @Override
    public void add(E element) {
        if(count == elements.length) {
            resize();
        }
            elements[count] = element;
            count++;
    }
    public void resize() {
        E newElements[] = (E[]) new Object[elements.length + elements.length/2];
        for(int i = 0; i < elements.length; i++) {
            newElements[i] = elements[i];
        }
        elements = newElements;
    }
    private class ArrayListIterator implements ListIterator<E> {
        private int currentPosition;
        @Override
        public E next() {
            E nextValue = elements[currentPosition];
            currentPosition++;
            return nextValue;
        }

        @Override
        public boolean hasNext() {
            return currentPosition < count;
        }
    }

        public ListIterator<E> iterator() {
            return new ArrayListIterator();
        }
    }
