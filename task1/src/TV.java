public class TV {
    private String model;
    private Channel[] channels;
    private int channelsCount;

    public TV(String model) {
        this.model = model;
        this.channels = new Channel[3];
        this.channelsCount = 0;
    }

    public void addChannel(Channel channel) {
        this.channels[channelsCount] = channel;
        this.channelsCount++;
    }

    public Channel[] getChannels() {
        return channels;
    }
}
