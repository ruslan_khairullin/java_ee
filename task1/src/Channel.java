public class Channel {
    private String name;
    Program[] program;
    int numbCount;

    public Channel(String name) {
        this.name = name;
        this.program = new Program[3];
        this.numbCount = 0;
    }
    public void addProgram(Program program) {
        this.program[numbCount] = program;
        this.numbCount++;
    }

    public Program[] getProgram() {
        return program;
    }

}